---
title: 友情链接
date: 2023-07-02 16:02:51
type: "link"
order: 5
---

<hr>
如需申请友情链接，请在本页留言，或通过<a href="/about/" target="_blank">关于页面</a>中的联系方式与我联系。

友情链接规则：
1、你的博客内容不存在违反法律法规、社会道德的情形。发现一次即永久取消友链，情节严重的将同时向警察叔叔举报。
2、你的博客内容最好以兴趣爱好、日常生活为主，不接受商业性质的站点。
3、你的博客至少被一个搜索引擎（Google、Bing、百度）正常收录。
4、你的博客应当处于正常更新维护状态。累计更新不满12个月的，连续3个月未更新将取消友链；累计更新满12个月的，连续6个月未更新将取消友链。
5、你的博客应当运行相对稳定。1个月内存在多次无法访问情形的，将被标记并置于末尾；连续3个月均无法访问的，将取消友链。
6、请按照下方表格所示的信息将我的博客添加到你的博客友情链接中。发现单方面取消友链的，将对等取消友链。
<div align="center"><table>
<tr><td>标题</td><td>Zhu's Blog</td></tr>
<tr><td>URL</td><td>https://azhu.site/</td></tr>
<tr><td>文字描述</td><td>记录赛博居民的日常折腾</td></tr>
<tr><td>LOGO(透明背景)</td><td>https://azhu.site/assets/img/favicon.png</td></tr>
<tr><td>LOGO(白色背景)</td><td>https://azhu.site/assets/img/favicon.jpg</td></tr>
<tr><td>RSS</td><td>https://azhu.site/rss.xml</td></tr>
</table></div>
