---
title: 如何将GiffGaff sim卡转换为esim
date: 2024-06-13 22:00:00
updated: 2025-02-24 01:07:00
categories: sim card
tags: [United Kingdom, esim]
cover: /post_img/2024/02/giffgaff-01.jpg
permalink: posts/1015/
excerpt: 无需Giffgaff APP和esim手机，借助HTTP请求工具PostMan获取esim二维码
---

　　本文介绍通过HTTP请求工具PostMan获取Giffgaff esim二维码的方法。

　　（本文截图较多，如加载缓慢请耐心等待或尝试刷新）

<img src="/post_img/2024/02/giffgaff-02.jpg" style="width: 100%; height: 250px; object-fit: cover;">

## 一、原理

　　Giffgaff是英国的一家虚拟运营商，其Giffgaff卡适合长期保号使用。（参考文章：<a href="/posts/1004/" target="_blank">英国GiffGaff手机卡购买和使用教程</a>）

　　Giffgaff原先只提供实体SIM卡，随后开始支持将实体SIM卡转换为esim或者直接购买新的esim。Giffgaff并不提供esim的二维码，而是通过自家的APP直接将esim配置文件下载到手机中。APP也会检测手机是否原生支持esim。（参考内容：<a href="https://www.giffgaff.com/help/articles/how-do-i-get-an-esim-on-giffgaff" rel="nofollow" target="_blank">GiffGaff官网 - 如何获取esim</a>）

　　对于使用5ber等实体esim卡的用户来说，需要想办法找一部原生支持esim的手机，在Giffgaff APP下载esim配置文件之前，通过网络嗅探工具“抓包”的方式获取esim配置文件的下载链接，然后将链接转换为二维码的形式，实现“曲线救国”。这种方法除了需要先找一部esim手机，还需要具备一定的动手操作能力，所以并不方便。

　　好在网上有大神写了PostMan脚本，通过HTTP请求的方式直接与Giffgaff的服务器进行通信来获取esim配置文件的下载链接，不依赖esim手机和Giffgaff APP。本文即介绍这个方法的操作步骤。

>声明
1、原脚本由<a href="https://www.nodeseek.com/post-76162-1" rel="nofollow" target="_blank">pwrli大佬</a>提供。由于原脚本中多处API改变了传递参数的方法，原脚本需多处手动操作才能正常使用。为了便利普通使用者，阿猪在其基础上做了少许修改以适配API的变化。此脚本最后更新于2025年02月23日。
2、阿猪亲自测试了这个脚本。在阿猪测试时，这个脚本是有效的，可以正常获取LPA、生成二维码。但是这不代表这个脚本对所有的计算机设备都兼容，也不代表这个脚本在未来不会失效。读者使用这个脚本，需自行承担可能产生的任何风险。
3、这个脚本中包含有固定的clientSecret和clientId值，如果使用人次较多，则存在被GiffGaff风控的潜在风险。
4、脚本使用了外部第三方的服务器来生成二维码，这意味着用户的esim下载链接会传输到外部第三方服务器，且存在被泄露或者被恶意利用的风险。读者可以使用离线的二维码生成工具自行生成二维码来规避这个风险。
5、出于自身能力和精力的限制，可能还存在阿猪没有发现的隐藏在脚本中的其他潜在的风险或BUG。读者使用这个脚本，需自行承担可能产生的任何风险。

## 二、操作步骤

### 1、安装PostMan客户端

　　打开<a href="https://www.postman.com/downloads/" rel="nofollow" target="_blank">PostMan官网的APP下载页面</a>，选择对应的操作系统进行下载。本文以Windows客户端为例。

　　首次运行PostMan会提示我们注册一个账号，或者使用已有的账号进行登录。自行注册一个账号，然后登录。

### 2、导入脚本

　　下载<a href="http://cdncontent.azhu.site/assets/2025/03/Giffgaff-swap-esim_20250304a.postman_collection.json" rel="nofollow" target="_blank">GiffGaff实体卡转eSIM的PostMan脚本</a>。（鼠标移到链接上方，点击右键，选择“链接另存为”）

　　回到PostMan，依次点击客户端左上角的“Menu” - “File” - “Import”，或者直接按下“Ctrl + O”快捷键，打开导入界面。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-01.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　点击“Select files”，浏览并选中刚才下载的脚本文件，或者直接将文件拖拽进窗口。

　　导入后，如下图所示。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-02.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

### 3、获取Access Token

　　要通过HTTP请求的方式直接与Giffgaff服务器通讯，首先需要获取一个Access Token。向服务器发送的请求中需要包含这个Token来验证用户身份。

　　如下图所示，依次点击“Authorization” - “Get New Access Token”。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-03.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　在弹出的登录窗口中输入自己的Giffgaff账号和密码，点击“Log in”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-04.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来在窗口中输入发送到Email或者手机号的验证码，点击“Log in”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-05.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　登录成功后，会看到如下图所示的Token Detail，点击“Use Token”按钮。此时相当于我们已经使用账号密码登录了Giffgaff APP。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-06.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

### 4、二次认证

　　点击左侧列表中的“发送认证邮件”，然后点击右侧的“Send”按钮，此时Giffgaff会向我们的EMail发送一个验证码。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-07.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　点击左侧列表中的“检查邮件认证码”，然后点击“Body”选项卡，如下图所示将收到的验证码填入`"code":`后边的双引号内，然后点击“Send”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-08.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　如果顺利的话，会返回一个包含“signature”字段的json。

　　点击左侧列表中的“获取会员资讯”，然后点击右侧的“Send”按钮做一个测试。如果一切正确的话，会返回一个包含各种用户信息的json，如下图所示。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-09.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

### 5、生成新的空卡

　　点击左侧列表中的“Reserve SIM”，然后点击右侧的“Send”按钮。这个步骤的作用是为esim预留一个编号，相当于新开了一张空白的SIM卡，用于将旧的实体SIM卡转移过来。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-10.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

### 6、换卡

　　点击左侧列表中的“Swap SIM”，然后点击右侧的“Send”按钮。这个步骤的作用是换卡，即弃用旧的实体卡，启用新的esim。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-11.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

>根据GiffGaff官网的说明，只能在英国当地时间4:30至21:30之间进行换卡操作。

### 7、生成esim配置文件

　　点击左侧列表中的“Get ESIMs”，然后点击右侧的“Send”按钮。这个步骤的作用是生成esim配置文件。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-12.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

### 8、获取eSIM下载地址

　　点击左侧列表中的“Get ESIMs”，然后点击右侧的“Send”按钮。这个步骤的作用是获取eSIM配置文件的下载地址。`lpastring`字段的值就是eSIM配置文件的下载地址，格式类似`LPA:1$服务器地址$MatchingID`。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-13.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

### 9、生成二维码

　　点击左侧列表中的“Get ESIM QRCode”，然后点击右侧的“Send”按钮。这个步骤的作用是将下载地址转换为二维码，方便手机扫描。

<div style="text-align: center;"><img src="/post_img/2024/06/giffgaff-14.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

>此处使用了第三方的二维码在线生成服务。如果担心信息泄露，可以选择自己信任的其他在线或者离线的二维码生成服务。

### 10、安装esim
　　使用支持eSIM的手机、5ber APP或者其他第三方的eSIM管理工具扫描这个二维码，即可下载并安装eSIM配置文件。（参考文章：<a href="/posts/1005/" target="_blank">5ber eSIM手机卡购买和使用教程</a>）

> 新的esim通常会在一个小时内有信号。如果一直没有信号，请尝试开启、关闭飞行模式，或者重启手机。