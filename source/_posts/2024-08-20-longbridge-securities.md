---
title: 长桥证券(香港)注册和使用教程
date: 2024-08-23 15:57:00
updated: 2024-10-29 14:03:00
categories: stocks
tags: [Hong Kong]
cover: /post_img/2024/08/longbridge-01.png
#sticky: 100
permalink: posts/1023/
excerpt: 港、美股终身免佣
---

　　长桥证券（香港），港、美股终身免佣。本文介绍注册流程、如何领取新户奖励以及主要功能的使用。
　　（本文截图较多，如加载缓慢请耐心等待或尝试刷新）

<img src="/post_img/2024/08/longbridge-02.jpg" style="width: 100%; max-height: 250px; object-fit: cover;">

## 一、简介

　　根据长桥证券官网的介绍，长桥证券在香港、美国、新加披、新西兰拥有证券相关业务牌照，接受香港证监会、美国证监会、美国金融业监管局、新加披金融管理局等的监管。<a href="https://longbridge.com/hk/about" rel="nofollow" target="_blank">**具体的许可事项请自行查阅官网，请自行鉴别真伪、自行承担风险。** </a>

　　新用户（指2024/09/01之前在长桥的任何账户均无入金、转仓记录的用户）在2024/09/01至2024/09/30期间累计净转入资产（现金入金+股票转仓）达标的，可在达标后的3到5个工作日内收到相应的奖励：
<table>
<thead>
<tr>
<th>达标条件</th>
<th>达标奖励</th>
<th>备注</th>
</tr>
</thead>
<tbody>

<tr>
<td>累计净转入资产达到等值1万港币</td>
<td>港美股终身免佣卡</td>
<td>-</td>
</tr>

<tr>
<td>累计净转入资产达到等值2,500美元</td>
<td>400港币股票现金卡<br>5美元期权现金卡</td>
<td>-</td>
</tr>

<tr>
<td>累计净转入股票（不含现金入金）达到等值1万美元</td>
<td>额外再奖励500港币股票现金卡</td>
<td>-</td>
</tr>

</tbody>
</table>

>　　通过阿猪的<a href="https://app.longbridgehk.com/ac/oa?account_channel=lb&channel=ZHR00004&invite-code=JYPSMW" rel="nofollow" target="_blank">邀请链接</a>或者邀请码`JYPSMW`注册并满足上述任一达标条件，还可额外得到100港币现金卡的渠道奖励（若未到账请私聊联系阿猪补发）。普通的邀请码是没有这个奖励的哦~

## 二、注册流程

>注：网站、APP中的界面、流程、功能等可能会随着时间推移而变化，本文的内容仅供参考。

>提前准备：
1、香港银行或海外券商的存量用户证明，或者海外IP地址。
2、一个可以向香港汇款的银行账户，用于入金。建议优先使用香港的银行。

### 1、注册账号

　　打开<a href="https://app.longbridgehk.com/ac/oa?account_channel=lb&channel=ZHR00004&invite-code=JYPSMW" rel="nofollow" target="_blank">邀请链接</a>，进入注册页面。点击页面最下方的“注册登录”按钮。

>也可直接从Google Play Store或者Apple APP Store下载`长桥`APP，通过APP注册，流程是一样的。但不要忘记手动填写邀请码`JYPSMW`，否则无法领取渠道奖励。

　　接下来进入“登录/注册”界面。

　　输入自己的手机号，点击“请输入验证码”旁边的“获取验证码”按钮。然后在“请输入验证码”一栏输入收到的短信验证码，勾选“我已阅读并同意服务协议...”，点击“登录”按钮。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-03.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 2、上传证件

　　接下来会看到如下图所示的提示，点击“同意并继续开户”。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-04.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来会提示我们准备好身份证和内地银行卡（用于对中国大陆用户进行身份验证，不用于出入金）。点击“立即开户”按钮。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-05.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来按照页面的提示上传自己的身份证照片正面和反面。如果你需要使用其他证件，请自行选择相应的证件签发国。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-06.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>　　对于中国大陆用户，还需要证明自己是（1）海外券商或者香港银行的存量客户，或者（2）当前在中国大陆之外居住、生活。
　　对于第一种方法，需要上传一份2024年06月01日之前的海外券商或者香港银行的日/月结单或者开户证明。
　　对于第二种方法，需要使用非中国大陆的IP访问注册页面，或者授权该页面获取位置信息，证明自己在海外。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-07.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 3、填写个人信息

　　接下来是填写个人基础信息、职业信息、资产信息、投资信息等，按照要求如实填写即可。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-08.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来是风险披露。勾选页面底部的“本人已经详细了解...”，点击“下一步，确认信息”。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-09.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来进入“确认信息”页面。核对信息是否无误，如果信息正确，则点击“下一步，身份认证”按钮。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-10.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 4、身份认证

　　接下来进入“身份认证/验证银行卡”页面。

　　首先会弹出一个“电子认证服务条款”，点击“确认并同意前述条款”。

　　接着填写自己的中国大陆银行卡的卡号、银行预留手机号等信息。完成后，点击“下一步，人脸验证”按钮。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-11.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>此处的银行卡号仅用于通过比对银行信息进行身份认证，不用于出入金。

　　接下来进入“身份认证/人脸验证”页面。点击“开始录制”按钮，将手机摄像头对着自己的面部录一个3秒左右的视频。验证通过后，点击“下一步，签名”按钮。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-12.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来进入“身份认证/签名”页面。使用手指在手机屏幕上写下自己的名字，点击“完成”按钮。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-13.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来会提示我们耐心等待审核，通常一个工作日内即可完成审核。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-14.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

## 三、确认邀请关系

>在首次入金之前，需要确认是否已经与阿猪绑定邀请关系，否则无法领取阿猪的渠道奖励。

　　从Google Play Store或者Apple APP Store下载并安装`长桥`APP，然后使用先前注册的账号登录长桥APP。也可从<a href="https://longbridge.com/hk/download" rel="nofollow" target="_blank">长桥证券的官网</a>下载<a href="https://static.lbctrl.com/app/longbridge-online-release.apk" rel="nofollow" target="_blank">APK文件</a>或者PC客户端。

　　在APP中依次点击“我的” - “邀请好友” - “绑定/更改邀请关系”，检查“入资邀请人”一栏是否已成功绑定阿猪。如未成功绑定，则需点击“绑定”按钮，输入阿猪的邀请码`JYPSMW`。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-15.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

## 四、入金

　　入金的本质是向长桥证券的对公账户进行转账，该账户开立在工银亚洲。香港的券商没有“三方存管”的概念，客户的资金都是存放在券商的对公账户内的。

### 1、选择入金币种

　　在APP中依次点击“资产” - “存入资金”，接着选择入金币种。目前支持港币和美元。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-16.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 2、选择入金银行

　　选择银行所在的国家/地区。
<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-17.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来选择入金的银行。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-18.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 3、选择入金方式

　　如果你是从香港本地的银行进行入金，则可以选择eDDA快捷入金、FPS(转数快)、网银转账、支票转账、银证转账等方式；如果你是从香港之外的银行进行入金，则只支持电汇的方式。
<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-19.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　在本文中，阿猪仅以常用的eDDA、FPS、网银转账为例进行演示，其他方式请读者自行尝试。

>1、请务必使用本人的同名银行账户，否则券商会拒绝为你入金。
2、通过香港的银行入金美元，长桥不收取任何费用，但是工银亚洲会收取约1.3美元/笔的手续费（香港的许多银行都会对非港币入账收取额外的手续费）。
3、阿猪建议使用香港的银行，可以零成本出入金港币。

### 4、eDDA快捷入金

　　首次使用eDDA（Electronic Direct Debit Authorization，电子直接扣款授权）快捷入金需要先进行一次授权，后续使用无需重复授权。

　　如下图所示，依次填写银行账户信息，勾选“本人已阅读并同意...”，然后点击“确认授权”按钮。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-20.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来，页面会提示我们“信息提交成功”。一般一两分钟分钟左右即可完成授权，完成授权后APP内会有推送消息进行提示。

　　再次重复前边的步骤，选择通过完成授权的银行账户进行eDDA入金。如下图所示，这次已经不需要进行授权了，直接输入入金的金额，然后点“确认存入”按钮即可完成入金。资金一般是实时到账。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-21.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 5、FPS入金

　　FPS（Faster Payment System，快速支付系统）是香港本地的一种主流支付系统。

　　下图是通过FPS向长桥证券转账的收款人账号信息。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-22.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　打开自己的银行APP，进入FPS转账的页面，填写长桥证券的FPS ID和转账备注，然后发起转账。

>1、请务必填写转账备注，否则需要人工审核，会极大的影响入金的效率。
2、不同银行APP的操作界面不尽相同，但操作流程基本相似，一般都是先添加收款人信息，然后再发起转账。此外一些银行的FPS转账可能会叫其他名称，比如南洋商业银行叫“快速转账”，如果找不到FPS转账功能请留意相似的名称。

　　在银行APP完成操作后，回到长桥APP，点击“立即上传汇款凭证”按钮，上传银行汇款成功的页面截图，然后点击“提交”按钮。资金一般是一分钟左右到账。

>在填写了转账备注的情况下，理论上无需上传汇款凭证亦可完成FPS入金。

### 6、银行转账

　　下图是向长桥证券进行银行转账的收款人账号信息。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-23.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　打开自己的银行APP，进入转账的页面，填写长桥证券的收款信息和转账备注，然后发起转账。

>1、请务必填写转账备注，否则需要人工审核，会极大的影响入金的效率。
2、不同银行APP的操作界面不尽相同，但操作流程基本相似，一般都是先添加收款人信息，然后再发起转账。

　　在银行APP完成操作后，回到长桥APP，点击“立即上传汇款凭证”按钮，上传银行汇款成功的页面截图，然后点击“提交”按钮。资金一般是一到两个工作日左右到账。

## 五、转入股票

### 1、填写转出信息

　　在长桥APP中依次点击“资产” - “全部功能” - “转入股票”。

　　接下来选择股票所属的市场。目前支持转入港股、美股。

　　接下来填写转出券商的信息，包括券商名称、账户姓名、账户号码。填写完成后，点击“下一步”按钮。

　　如果转出的券商不在列表中，还需补充提交券商的参与者代号（中央结算编号）、联系方式等（可登录转出券商的APP或者联系其客服获取）。

　　接下来填写需要转入的股票信息，包括股票代码、数量。填写完成后，点击“下一步”按钮。

　　确认填写的信息无误后，继续点击“下一步”按钮。

### 2、通知转出券商

　　接下来登录转出券商的APP，找到“转出股票”的页面，然后按照提示填写自己在长桥证券的账户信息，通知转出券商咱们要转出股票。

　　完成上述步骤后，回到长桥APP，点击“我已通知”按钮。一切顺利的话，耐心等待3-5个工作日股票即可到账。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-24.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>从其他香港券商向长桥证券转入股票，长桥证券不收取转入手续费，但是转出的券商一般会收取转出手续费，具体费率请咨询转出的券商。

## 六、买卖股票

（略）

## 七、兑现新户奖励

　　新用户入金、转仓达标后，会收到股票现金卡、港美股终身免佣卡。有时也会额外发放现金卡。

　　在长桥APP中依次点击“我的” - “我的卡券”可以看到已经发放的奖励。

<div style="text-align: center;"><img src="/post_img/2024/08/longbridge-25.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 1、使用终身免佣卡

　　港股、美股下单结算时，系统会自动免除佣金，无需用户进行额外的操作。

### 2、使用现金卡

　　点击现金卡卡面上的“使用”按钮，系统会自动将对应金额的现金发放至用户的账户。

### 3、使用股票现金卡

　　买入股票、ETF时，下单页面会自动显示出可抵扣的金额。

　　下单时仍需全额支付款项，待完成清算后，抵扣的金额会自动返还到用户的账户。

>注：货币基金等部分品种不支持使用股票现金卡，此时下单页面不会显示可抵扣金额。

## 八、出金

　　在长桥APP中依次点击“资产” - “全部功能” - “提取资金”。

　　依次选择提款的币种、收款的银行账户，输入提款的金额，然后点击“确定提取”按钮。接着耐心等待即可。

>1、出金至香港的本地银行，通常一个工作日内即可到账。长桥不收费，但出金美元时，部分收款银行会对非港币收取入账手续费。
2、出金至海外的银行是以电汇的形式进行，通常3至7个工作日到账。长桥不收费，但汇出行会收取140港币的固定费用，需由用户承担。如果有中转行，中转行费用也由用户承担。此外一些收款行可能会对国际汇款或者非本币收取入账手续费。