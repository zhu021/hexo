---
title: 如何使用国内的银联卡订阅ChatGPT
date: 2024-03-06 23:00:00
updated: 2024-03-06 23:00:00
categories: others
tags: [gpt, card, UnionPay]
cover: /post_img/general/chatgpt.jpg
permalink: posts/1009/
excerpt: 在Google Play中绑定国内的银联卡，通过应用内购买间接实现国内的银联卡订阅ChapGPT。
---

　　本文介绍借助Google Play的“应用内购买”间接实现国内的银联卡订阅ChatGPT的方法。

<img src="/post_img/general/chatgpt.jpg" style="width: 100%; height: 250px; object-fit: cover;">


## 一、准备Google账号

　　据网友们的经验传授，香港、澳门、新加坡、泰国、菲律宾的Google Play支持绑定国内银行发行的银联卡。但是ChatGPT不向香港、澳门提供服务，所以需要准备一个新加坡、泰国或菲律宾的Google账号。

　　如果你的Google账号不属于以上几个地区，可以尝试以下方法：

1、改变使用环境，触发Google Play的“变更国家/地区”选项。

　　Google会通过IP地址、定位信息判断用户所处的地区。长期在一个新的地区使用，有较大的概率会触发Google Play的“变更国家/地区”选项。

> 短期内多次变更使用环境或修改“国家/地区”信息，可能会触发Google的风控，导致封号。

2、直接在新的使用环境中注册一个新的Google账号

## 二、应用内购买

　　使用Google账户登录ChatGPT安卓客户端。

　　点击左上角的双横杠、然后点击右下角的三个小点，进入Setting界面。接下来点击Settings界面中的Subscription选项，然后点击Subscribe按钮。

　　此时会弹出Google Play的应用内购买界面。在购买界面中点击付款方式右侧的箭头，进入“付款方式”管理界面。在“付款方式”中点击“添加信用卡或借记卡”，依次输入国内银联卡的卡号、有效期、安全码、账单地址（不建议填中国地址），然后点击“保存卡”按钮。

　　接着回到“付款方式”界面中选中刚才添加的卡，然后点击“订阅”按钮即可。


> 付款后，每个月会自动续订。如果不打算继续使用，记得在Play商店中取消订阅。
