---
title: 菲律宾Maya Bank注册和使用教程（上）
date: 2024-01-20 23:00:00
updated: 2024-01-20 23:00:00
categories: 
 - [bank]
 - [ewallet]
 - [Cryptocurrency]
tags: [Philippines, card, visa, master]
cover: /post_img/2024/01/maya-bank-01.png
permalink: posts/1001/
excerpt: 菲律宾用户量最大的数字银行，可申请实体/虚拟银行卡，中国护照可注册。
---

　　本文介绍Maya Bank的注册和使用。
<img src="/post_img/2024/01/maya-bank-01.png" style="width: 100%; height: 250px; object-fit: cover;">

## 一、功能简介

　　Maya Bank是菲律宾的一家数字银行，无实体网点，接受菲律宾央行的监管，每个存款人享有50万比索（约合人民币6.46万元）的存款保险。

　　Maya的前身是电子支付工具PayMaya，天然具备互联网基因。虽然不如背靠蚂蚁金服的GCash主流，但是基本上GCash该有的功能都有，整体用户体验也不错。与其他菲律宾的银行、电子钱包相比，Maya对外国用户相对更友好一些，申请较为容易，方便外国人在菲律宾旅行、生活期间享受本地电子支付的便利。


## 二、注册和认证

　　<a href="https://www.maya.ph/app/registration?invite=K48RDM1EV16D" target="_blank"  rel="nofollow">点击这里进入Maya Bank的官方网站</a>，通过苹果App Store、谷歌Play Store或者华为应用商店下载并安装Maya Bank APP。

　　运行APP后，点击“Start an account”。依次输入自己的First Name(名字)、Last Name(姓)、EMail，并点击“Continue”。

　　接下来输入自己的手机号，并设置登录密码，然后点击“Continue”。

>　　Maya只支持菲律宾手机号注册。如果没有菲律宾手机号，可以考虑申请一个esim

使用Tinbo提供的虚拟手机号。点击“I don't have a Philippine SIM”，APP会在浏览器中打开Tinbo的网站。点击“Register Now For Free”，注册可以获得一个虚拟手机号，可以在Tinbo网站中接收短信。
　　菲律宾的手机号码实行实名制，从Tinbo获取的手机号也不例外。需要按照网站的提示填写个人信息，然后将自己的证件照和自拍照发邮件给运营商的客服。号码会在审核通过后生效，前3个月免费，之后需要付费1美元（不清楚是一次性费用还是需要每隔一段时间续费一次）。

　　接下来是“Data Privacy”页面，直接点击“Continue”即可。

　　接下来是“Data personalization”页面，直接点击“Save”即可。

　　接下来会验证你的手机号，输入收到的验证码，然后点击“Verify”即可完成注册。

>　　如果收不到短信验证码，请尝试：
（1）对于iPhone用户，将地区改为菲律宾，将语言改为英语；或者改用安卓手机进行注册。
（2）更换漫游网络的运营商。
（3）通过其他渠道向自己的手机号发送一条短信，确认手机可以正常接收短信，然后尝试重新发送Maya的短信验证码。


　　注册后的认证状态为“Standard”，需要点击“Upgrade”升级认证状态才能使用完整功能。

　　按照提示填写自己的个人信息，提交后耐心等待审核，审核通过后，即可正常使用Maya的完整功能了。

>1、使用邀请码`K48RDM1EV16D`注册Maya Bank，除了常规的新人福利，还能额外享受通过InstaPay对外转账前3个月免手续费的优惠。（优惠信息仅供参考，以官方的活动规则和交易页面实际显示的费用金额为准。<a href="https://www.maya.ph/terms-and-conditions" target="_blank"  rel="nofollow">参考信息：常规收费标准</a>）
2、“First Name”对应中文的“名”，“Last Name”对应中文的“姓”，否则审核结果会提示名字与ID中的名字不一致。
3、“Country of residence”要选择“Philippines”。Maya只对菲律宾人和住在菲律宾的外国人提供服务。
4、“Present address”填写自己在菲律宾的居住地址即可，游客可以填写自己的酒店地址。
地址中的“Barangay”类似于国内的街道。在大城市，“Barangay”一般同时有数字编号和名称，如果查不到地址对应的“Barangay”，可以尝试使用街道的名称代替。
5、“Permanent address”不要勾选“同Present address”，否则审核结果会提示漏填信息。“Permanent address”需要单独填写，填写自己的国内地址即可。


## 三、充值、入金

1、通过储蓄卡、信用卡充值

　　支持通过Visa、Master、JCB卡充值，每次收取200比索（约合人民币26元）的手续费。

2、通过国际汇款
　　Wise、Paysend等一些汇款渠道都支持汇款至GCash、Maya等菲律宾本地的电子钱包账户，也支持汇款至当地的银行账户，具体的费用取决于你使用的汇款渠道。
　　推荐通过Paysend向菲律宾银行账户转账的方式给Maya入金，免手续费，到账快，汇率也可以。收款账户填写Maya的活期Savings账户即可。

3、通过线下便利店、自助设备充值

　　Maya支持通过合作的线下门店或自助设备进行充值，一般收取0%至2%的手续费。

　　在Maya APP中点击“Cash In”，然后在“Our Partners”中选择一个线下充值方式，输入你想要充值的金额，点击“COntinue”。然后按照APP的提示前往线下门店，告知店员或者在自助设备中输入“Cash in code”，并支持现金即可完成充值。

4、通过支持QR PH的电子钱包向自己的Maya钱包转账。
　　可以在淘宝、咸鱼上找卖家代充，但是价格偏高，不推荐。

5、接收加密货币，然后卖出。
　　Maya目前支持24种市场主流的加密货币，但支持的网络较少。
　　从外部接收加密货币后，点击“卖出”即可将加密货币兑换为菲律宾比索，资金会进入Maya的钱包账户内。

6、通过当地的银行向自己的Maya Saving Account转账。

7、关联当地的银行账户，直接从银行账户中扣款。

## 四、日常消费

1、扫码付款

　　菲律宾的主流移动支付协议是政府主导的QR Ph，使用很普遍。只要收款码中带有蓝黄红色的QR Ph Logo就可以使用Maya进行扫码付款。

<div style="text-align: center;"><img src="/post_img/2024/01/maya-bank-02.webp" alt="" style="width: auto; height: auto;"></div>

2、刷卡消费

　　Maya提供虚拟的Master和实体的Visa、Master储蓄卡，可用于网上支付、线下刷卡消费、ATM取款。虚拟卡免费，实体卡的价格是200比索，可以邮寄到你的菲律宾地址。
<div style="text-align: center;"><img src="/post_img/2024/01/maya-bank-03.webp" alt="" style="width: auto; height: auto;"></div>

3、交通出行

　　Maya APP上可以直接购买LRT1轻轨的车票，也可以给beep交通卡充值。

4、支付账单

　　Maya APP支持直接为手机号、宽带、信用卡、水电费、保险、过路费等支付账单。

5、游戏充值

　　可以直接购买LOL、XBox、Steam等游戏或游戏平台的充值券。

<br>

--------------------------------------------

{% raw %} >>> 请点击此处继续浏览：{% endraw %}

## <a href="/posts/100a/" target="_blank"><b> 菲律宾Maya Bank注册和使用教程（下）</b></a>