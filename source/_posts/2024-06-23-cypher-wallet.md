---
title: Cypher Wallet注册和使用教程
date: 2024-06-24 22:00:00
updated: 2024-09-06 11:15:00
categories:
 - [ewallet]
 - [Cryptocurrency]
tags: [Panama, United States, card, Master]
cover: /post_img/2024/06/cypher-01.jpg
permalink: posts/1017/
excerpt: 非托管的加密货币钱包，可申请实体/虚拟Master卡，支持全套中国资料注册
---

　　本文主要介绍Cypher Wallet中的Master卡的注册和使用方法。该钱包的其他功能与True、MetaMask等常见的非托管型钱包大同小异，不做主要介绍。
　　（本文截图较多，如加载缓慢请耐心等待或尝试刷新）

>Cypher从2024年9月起更换了合作的发卡机构，新的发卡机构不支持中国证件进行KYC。

<img src="/post_img/2024/06/cypher-01.jpg" style="width: 100%; height: 250px; object-fit: cover;">

## 一、功能简介

　　CypherD Wallet Inc是Y Combinator投资的一家初创公司，公司位于美国加利福尼亚的Palo Alto，团队成员约有10人。Cypher Wallet是该公司出品的一个非托管加密货币钱包，并提供“非托管”的实体/虚拟加密货币Master卡，支持全套中国资料注册。该卡的发卡机构为CANAL BANK S.A.，卡BIN归属地为巴拿马，卡类型为信用卡，卡等级为GOLD。

　　使用Cypher卡（下称“小狗卡”），首先需要在Cypher Wallet APP中打开一个链上钱包（可以新创建一个钱包，可以使用助记词或密钥导入一个现有的钱包，也可以与其他的钱包程序进行关联），然后在APP中申请一张小狗卡，接着再在APP中为小狗卡充值（即从打开的钱包中向Cypher进行链上转账）。

>出于资金安全的考虑，建议大家每次不要给小狗卡充值太多余额，用完再充。

## 二、打开一个链上钱包

　　打开Cypher Wallet APP，在出现的介绍页面中点击“SKIP”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-03.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　根据自己的情况或者喜好选择一个打开钱包的方式。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-04.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 1、创建一个新的钱包

　　如果没有现成的钱包，或者希望单独创建一个新的钱包用于给小狗卡充值，可以点击“Create New Wallet”按钮创建一个新的钱包。

　　接着选择助记词（Seed Phrase）的长度，12个或者24个随意。等待APP生成助记词后，用另一个手机对着屏幕拍照记下来，下一步会用到。以后在其他钱包程序导入这个钱包的时候也需要用到。

　　接着点击“Confirm”按钮，按照之前生成的助记词的顺序选择相对应的单词。这个步骤的作用是验证你已经记住了助记词，也可以直接点击“Skip For Now”跳过。

>经阿猪测试，有些设备与Cypher Wallet存在兼容性问题，新建或导入钱包后无法正常打开APP。如遇此情形，请尝试更换设备，或者改为通过关联外部钱包的方式打开一个钱包。

### 2、导入一个现有钱包

　　如果想直接使用自己现有的钱包，可以点击“Import With Seed Phrase”或者“Import With Private Key”，按照APP的指引将一个现有的钱包导入到Cypher Wallet中。

### 3、关联其他的钱包程序

　　如果你的设备中已经安装了MetaMask、Trust、Coinbase Wallet等其他的钱包APP，也可以点击“Connect A Wallet”，按照指引在其他的钱包程序中授权Cypher Wallet访问和操作其他钱包中的加密币。

>提示：
　　小狗卡支持同时绑定多个不同的钱包地址。
　　在APP的Card界面中依次点击“Options” - “Linked Wallet”，然后点击右上角的“+ Link Another Wallet”，输入钱包的地址和名字，然后带年纪“UPDATE”按钮。下次在Cypher Wallet中导入或者关联这地址的钱包，即可使用该钱包中的资产为小狗卡充值了。

## 三、申请小狗卡

　　完成前边的步骤后，会进入APP的首页，如下图所示。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-05.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 1、获取邀请码

　　点击APP底部的“Card”图标，切换到卡功能界面。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-06.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　选择自己的国家，填写自己的EMail地址，然后点击“Join Waitlist”按钮。

　　此时APP会提示“Your request for the cypher card is successful”，表面Cpher已经给我们发出了邀请码。打开自己的电子邮箱，可以在EMail中的看到发来的邀请码。

>提示：
1、不同国家的邀请码互不通用。
2、请确保所选择的国家与自己KYC证件的发行国家一致，否则无法通过身份认证。

### 2、提交申请

　　再次回到刚才的Card界面，点击“Join Waitlist”按钮下方的“I have a invite code”链接。在如下图所示的界面的填入自己收到的邀请码，然后点击“Next”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-07.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　勾选“I confirm that ...”，然后点击“YES, GET STARTED”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-08.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　选择自己的国家，填写自己的手机号、姓名、生日、EMail，选择底部的“No”，然后点击“Next”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-09.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>提示：
1、这里的国家指的是我们用于KYC的证件的发行国家，不是自己居住的国家。
2、手机号的归属地不要求与第一行的国家保持一致。
3、经阿猪实测和网友的反馈，+86的中国手机号很难收到短信验证码。建议直接填写国外手机号码。

　　填写自己的账单地址，然后点击“Next”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-10.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　输入电子邮箱中收到的验证码，验证EMail地址。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-11.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　输入手机收到的短信验证码，验证手机号。
　　经阿猪实测和网友的反馈，+86的中国手机号很难收到短信验证码。如果你也收不到短信验证码，请尝试更换其他国家的手机号。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-12.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>当前步骤仅能更改同一国家的不同手机号，不能更换国家。请点击“Verify Later”暂时跳过手机号验证，待完成身份认证后在APP的“Options”中修改个人资料，改为其他国家的手机号，然后继续验证手机号。

　　点击“Proceed”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-13.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　再次点击“Proceed”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-14.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 3、KYC

　　点击“INITIATE KYC”按钮，APP会打开手机中的浏览器，跳转到外部的KYC认证网站。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-15.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　点击“Agree and Continue”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-16.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　依次选择自己证件的发行国家和用于KYC的证件类型。点击“Choose a document”，上传自己的证件照。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-17.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　点击“Open camera”，开始录像，头部对着摄像头转动两圈，点击停止按钮，上传视频。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-18.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　如下图所示，卡申请进入等待审核状态。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-19.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

## 四、用卡

　　待审核通过后，Card界面会变为如下图所示。Cypher默认免费提供一张虚拟卡。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-20.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　点击“Card Detail”按钮，可以看到卡号、有效期、CVV。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-21.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　在支持Master的商户网购时，输入自己的卡信息即可完成支付。也可绑定微信、Google Pay、Apply Pay、Paypal等第三方支付工具使用。

　　经阿猪实测，使用小狗卡消费非美元时，不收取额外的货币转换费。但是如果消费金额较小，可能会在换算为美元时产生四舍五入的损失。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-22.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　点击“Lock”按钮，可以临时锁定小狗卡，避免被盗刷。

　　当累计充值满500美元后，可以申请一张实体卡。申请实体卡需要支付运费，寄送到美国境内需要支付30美元，寄送到美国境外需要支付50美元。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-23.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

## 五、充值

　　点击Card界面右上角的“Load Card”按钮，会列出当前打开的钱包中的所有的币种和余额。

　　如果币种和所在的网络可用于为小狗卡充值，且该币种的余额不低于10美元，则该币种是彩色可选择的状态，否则是如下图所示的灰色不可选择的状态。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-24.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　本文以Base网络上的USDC币为例，在“Load Card”界面的币种列表中选择Base网络上的USDC，然后点击“Quote”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-25.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　APP会根据合作方的报价计算出我们选择的币种和数量对应的美元价值，即充值的金额，并给出预估的GAS费和转账时间。确认无误后点击“LOAD”按钮。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-26.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>提示：
1、向小狗卡充值的本质就是向Cypher的指定钱包地址进行转账，所以也是需要支付GAS费的。你需要确保自己钱包中拥有足够的对应网络的原生币用于支付GAS费，否则会看到类似上图所示“Insuffcient ETH to pay gas fee”的提示。
2、建议使用USDC、USDT等主流的稳定币，以及Poligon、Base等费用较低的网络。

>小常识：
加密币按网络类型可分为Layer1和Layer2两类。Layer1是自有的原生网络，的主要有BTC、ETH、Polygon、LTC等；Layer2则是依托于某一个原生网络。例如Base网络属于Layer2网络，它是基于ETH这个Layer1网络的，ETH网络的原生币是ETH，所以走Base网络的交易需要支付ETH币作为GAS费；Polygon网络属于Layer1网络，Ploygon网络的原生币是Matic，所以走Polygon网络或者基于Polygon网络的Layer2网络的交易需要支付Matic币作为GAS费。

　　耐心等待转账完成后，可以在Card界面中看到刚才的充值记录，卡的余额也相应增加了。

<div style="text-align: center;"><img src="/post_img/2024/06/cypher-20.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　如果钱包的余额不足以为小狗卡充值，可以先为钱包充值，然后再为小狗卡充值。

　　点击APP左下角的“Portfolio”标，选择一个打算充值的币种，点击“Receive”按钮，查看钱包地址。然后从自己的交易所账号或者其他非托管钱包向这个钱包地址转账。

>转账前注意核对收款的钱包地址，并确保收款的钱包支持自己所选择的网络，否则会导致转账失败、资产会丢失。