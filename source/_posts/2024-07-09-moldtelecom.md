---
title: 摩尔多瓦Moldtelecom预付费手机卡购买和使用教程
date: 2024-07-09 21:00:00
updated: 2024-07-09 21:00:00
categories: sim card
tags: [Moldova, Europe, esim]
cover: /post_img/2024/07/moldtelecom-01.jpg
permalink: posts/1021/
excerpt: 开卡成本40MDL（约合人民币16.45元），最低可2MDL（约合人民币0.82元）保号一年。支持eSIM，无需实名登记。
---

　　本文介绍摩尔多瓦Moldtelecom预付费eSIM手机卡的购买和使用。（本文截图较多，如加载缓慢请耐心等待或尝试刷新）

<img src="/post_img/2024/07/moldtelecom-01.jpg" style="width: 100%; height: 250px; object-fit: cover;">

## 一、资费介绍

　　Moldtelecom是摩尔多瓦的一家电信运营商，支持网上直接开通预付费ESIM，无需实名登记。开卡需购买40MDL（约合人民币16.45元）的套餐，最低可2MDL（约合人民币0.82元）保号一年。

### 1、漫游费率

　　以中国为例，漫游的资费价格如下：

<table>
<thead>
<tr>
<th>漫游至中国</th>
<th>价格（1MDL≈0.4086CNY）</th>
</tr>
</thead>
<tbody>

<tr>
<th>拨打电话至中国和摩尔多瓦</th>
<th>6MDL/分钟</th>
</tr>

<tr>
<th>拨打电话至其他国家</th>
<th>12MDL/分钟</th>
</tr>

<tr>
<th>接听电话</th>
<th>6MDL/分钟</th>
</tr>

<tr>
<th>发送短信</th>
<th>2MDL/条</th>
</tr>

<tr>
<th>接收短信</th>
<th>免费</th>
</tr>

<tr>
<th>流量</th>
<th>6MDL/MB</th>
</tr>

</tbody>
</table>

### 2、保号

　　在有效期内，可以正常使用服务。有效期结束后，进入6个月的宽限期。宽限期内只能接收短信、接听电话、拨打Moldtelecom useful numbers(200, 300, 700, etc.)和紧急电话112。宽限期结束后，号码会被收回。

　　通过充值可以延长有效期。以下为网友分享的信息，未验证，仅供参考：

<table>
<thead>
<tr>
<th>充值金额</th>
<th>延长有效期</th>
</tr>
</thead>
<tbody>

<tr>
<th>小于25MDL</th>
<th>15天</th>
</tr>

<tr>
<th>25MDL至49MDL</th>
<th>60天</th>
</tr>

<tr>
<th>50MDL及以上</th>
<th>180天</th>
</tr>

</tbody>
</table>

　　每次最小充值金额为1MDL，所以理论上每隔6个月充值1MDL即可保号，有效期会延长15天，同时宽限期相应顺延6个月。（有效期和宽限期仅从充值之日起顺延，不叠加。）

## 二、购买实体SIM卡/eSIM

### 1、实体卡

　　可以在摩尔多瓦的Moldtelecom门店、加油站、邮局、报刊摊、烟草店购买实体SIM卡。

### 2、eSIM

　　打开<a href="https://moldtelecom.md/esim" rel="nofollow" target="_blank">Moldtelecom官网的预付费ESIM购买链接</a>。购买页面提供罗马尼亚语和俄语选项，我们可以使用浏览器自带的翻译功能将页面翻译为英文。

　　点击黄色的“Activate Now”按钮，进入申请流程。

　　继续点击红色的“Activate eSIM”按钮。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-02.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　在“e-mail”文本框中输入自己的EMail地址（用于接收eSIM二维码），点击“Send code by email”按钮。在“the code from the email”文本框中输入EMail中收到的验证码，然后点击“Validation”按钮。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-03.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　选择一个自己喜欢的号码。如果没有喜欢的号码，可以点击“Other Numbers”更换一组号码。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-04.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

>　　如果不显示号码，可能是暂时售罄了，请尝试过一半天再重新尝试。

　　此处显示的是我们购买的号码对应的套餐，这个在购卡时无法取消。套餐下边时可选的附加资源包，无需选择。点击底部的“Keep going”按钮。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-05.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　此处显示的是套餐和已选择的附加资源包的加总金额。我们不选择任何附加资源包，总金额是40MDL。点击底部的“Pay”按钮，进入支付界面。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-06.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　在支付界面依次输入银行卡上的持卡人姓名、卡号、有效期、CVV码，然后点击底部的“PAY”按钮。支持Visa、Master、Google Pay、Apply Pay。

　　支付成功后，会看到如下页面。此时已经购买成功，Moldtelecom会向我们的EMail中发送账单信息和一封包含eSIM二维码的邮件。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-07.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

## 三、安装eSIM

　　打开邮箱，找到包含eSIM二维码的邮件。

　　使用支持eSIM的手机扫描这个二维码，下载并安装eSIM的配置文件。

>经测试，5ber可以正常安装和使用Moldtelecom的eSIM。（参考内容：<a href="/posts/1003/" target="_blank">5ber可以使用的eSIM</a>。）

## 四、实名登记

　　根据摩尔多瓦的法规政策，使用手机卡无需实名认证。

## 五、激活

　　将实体SIM卡插入手机或者启用eSIM，耐心等待手机卡注册到运营商的网络。当手机出现信号后，SIM卡即完成激活。

## 六、管理号码

### 1、注册账号

　　打开<a href="https://my.moldtelecom.md/my-moldtelecom/" rel="nofollow" target="_blank">My Moldtelecom的Web端登录界面</a>。（也可以在Google Play Store或者Apple Store中下载My Moldtelecom的APP，操作流程相似，固不再赘述。）

　　首次登录账号需要进行注册。点击“Sign up”按钮。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-08.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　点击“Phone”输入自己的手机号码，或者点击“Contract”输入自己的Contract号码。手机号码和Contract号码都可以在之前购买手机号时收到的EMail中看到。接下来点击“Next”按钮，此时系统会向我们的手机号发送一条短信验证码。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-09.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　在“Verification code”一栏填入收到的短信验证码，在“Password”和“Re-enter the password”一栏输入自己设置的登录密码，然后点击“Sign up”按钮。此时已完成注册。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-10.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　再次回到<a href="https://my.moldtelecom.md/my-moldtelecom/" rel="nofollow" target="_blank">My Moldtelecom的Web端登录界面</a>，输入用户名和密码，然后点击“Login”按钮。其中用户名是我们的Contract号码。

### 2、查看号码有效期

　　登陆后，界面如下图所示。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-11.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　页面的中心区域显示的就是当前号码的信息。Status表示号码当前的状态，Active till表示号码的有效期，Grace period表示号码的宽限期。

### 3、充值

　　点击页面左下角的“Pay for services”按钮。

　　在页面左上角勾选需要充值的号码，输入充值的金额，点击“Pay with credit card”按钮，进入支付界面。

<div style="text-align: center;"><img src="/post_img/2024/07/moldtelecom-12.jpg" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

　　在支付界面依次输入银行卡上的持卡人姓名、卡号、有效期、CVV码，然后点击底部的“PAY”按钮。支持Visa、Master、Google Pay、Apply Pay。

　　支付成功后，号码信息会相应的更新。经测试，号码的有效期、宽限期不会累积，只是从充值当日重新顺延。

>可以直接在官网的首页点击<a href="https://achitari.moldtelecom.md/Pay" rel="nofollow" target="_blank">Reincarcare cont图标</a>进行充值，无需注册、登录账号。登录账号只是为了更方便的查看号码信息。