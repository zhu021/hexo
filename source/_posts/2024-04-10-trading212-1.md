---
title: 英国券商Trading 212注册和使用教程（上）
date: 2024-04-10 22:00:00
updated: 2024-09-25 14:17:00
categories: stocks
tags: [United Kingdom, card, master]
cover: /post_img/2024/04/trading212-01.png
#sticky: 200
permalink: posts/1012/
excerpt: 新用户送最高100英镑的股票。可以交易英、美、加、欧多国的股票、ETF、外汇、大宗商品等，有借记卡。
---

　　新用户送最高100英镑的股票。可以交易英、美、加、欧多国的股票、ETF、外汇、大宗商品等，提供借记卡。<!-- more -->本文介绍注册流程以及主要功能的使用。

　　（本文截图较多，如加载缓慢请耐心等待或尝试刷新）

<img src="/post_img/2024/04/trading212-01.png" style="width: 100%; height: 250px; object-fit: cover;">

## 一、简介

　　Trading 212可以交易英国、美国、加拿大、欧洲多个证券交易所的股票、ETF，也可以交易外汇、大宗商品等。

　　根据Trading 212官网的介绍，Trading 212在英国、澳大利亚、保加利亚、塞浦路斯有公司实体，接受英国Financial Conduct Authority、澳大利亚Securities and Investments Commission、保加利亚Financial Supervision Commission、塞浦路斯Securities and Exchange Commission的监管。<a href="https://www.trading212.com/about-us" rel="nofollow" target="_blank">**具体的许可事项请自行查阅官网，请自行鉴别真伪、自行承担风险。** </a>

>　　（活动已结束）~从2024年09月24日起至2024年11月05日止，通过<a href="" rel="nofollow" target="_blank">邀请链接</a>注册Trading 212的新用户，在账户创建之日起的10日内入金1英镑及以上，即可随机获得最高100英镑的股票（一般不会低于20英镑）。股票将在达标后的3个工作日内到账，30日后可提现。
　　如果未通过邀请链接注册，将无法获得新用户奖励。可尝试依次点击“设置” - “Use promo code”，然后手工输入邀请码`17tKeGY7uS`。~

## 二、注册流程

>事先准备：
1、英国手机号，用于注册过程中接收短信验证码。（参考：<a href="/posts/1004/" target="_blank">英国GiffGaff手机卡购买和使用教程</a>）
2、英国地址，用于注册过程中填写个人信息。寄卡地址可与注册地址不同。
3、中国护照，或者Trading212支持的其他身份证件。
4、一个可以向英国汇款的银行账户或者一张Visa、Master储蓄卡，用于入金。建议优先使用欧洲的银行。（参考：<a href="/posts/1010/" target="_blank">英国iFAST Global Bank数字银行注册和使用教程</a>）

### 1、安装APP

　　在苹果App Store、谷歌Play Store或者其他应用商店搜索`Trading 212`，然后下载并安装。

　　也可直接通过<a href="https://www.trading212.com/invite/17tKeGY7uS" rel="nofollow" target="_blank">邀请链接</a>进行注册。

### 2、注册

　　打开App，点击“Open account”按钮。

　　在“Country of residence”页面中选择自己的居住国。不同的居住国，可能会对应不同的服务可用性和法规政策，具体请查阅官网的帮助文档。本文以英国为例，选择“United Kingdom”。

　　在“Select account”页面选择开户的账户类型。Trading 212目前提供Invest、CFD、ISA三种账户类型。Invest账户主要用来购买股票和ETF；CFD账户在购买常规股票的基础上，可以进行多空的杠杆交易；ISA账户(Individual Savings Accounts)内的储蓄或者股票投资可以享受英国资本利得税和个人所得税的减免。

　　本文以Invest账户为例，选择“Trading 212 Invest”。其他的账户类型可以在日后再添加，但是请根据自己的投资经验和税务居民身份谨慎添加。

　　在“Login Details”页面输入自己的EMail和密码，勾选“I accept the Privacy Policy ...”，并点击“Creat account”按钮。

　　接下来，我们会来到APP的首页。至此，我们已经完成了Trading 212 账户的注册。

### 3、设置账户

　　如图所示，点击“Set up your account”，继续完善个人信息。

<div style="text-align: center;"><img src="/post_img/2024/04/trading212-02.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　在“Nationality”页面选择自己的国籍，然后点击“Next”按钮。

　　在“Name as in passport”页面填写自己的姓名，然后点击“Next”按钮。其中“First Name”为名，“Surname”为姓。

　　在“Date of birth”页面填写自己的出生日期，然后点击“Next”按钮。格式为“日/月/年”。

　　在“Phone Number”页面填写自己的手机号码，然后点击“Next”按钮。此处的手机号码的归属国需要与注册账号时的“Country of residence”保持一致。

　　在“Tax residence”页面填写自己的税务居民信息，然后点击“Next”按钮。根据自己的情况如实填写即可。

<div style="text-align: center;"><img src="/post_img/2024/04/trading212-03.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　在“National Passport Number”页面填写自己的护照号码，然后点击“Next”按钮。（因为之前居住国选择的是英国，国籍选择的是中国，所以此处要求填写提供的证件是护照。）

　　在“Tax ID”页面填写自己的税号。（因为之前选择的税务居民为中国，所以此处填写的是中国税号，即中国身份证号码。）

　　在“Address”页面填写自己的居住地址。可以直接在搜索框中填写自己的邮编，然后在搜索结果中选择自己的地址；也可以点击底部的“Enter address manually”，手工填写自己的地址。

<div style="text-align: center;"><img src="/post_img/2024/04/trading212-04.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来是好几个关于财务信息的问题，例如出入金方式、投资目标、风险承受水平、雇佣状态、记账币种、投资经验等，按照自身的情况如实选择即可。

　　在“Terms and policies”页面依次查阅服务条款，然后点击“I confirm”按钮。

<div style="text-align: center;"><img src="/post_img/2024/04/trading212-05.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　此时我们会看到“账户设置完成”的提示。

### 4、身份认证

　　继续点击“Next”按钮，进入身份认证流程。

<div style="text-align: center;"><img src="/post_img/2024/04/trading212-06.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　在“Choose your document”页面选择自己身份证件的签发国家和证件类型。本文中选择的是中国的护照。

<div style="text-align: center;"><img src="/post_img/2024/04/trading212-07.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　按照APP的提示对证件完成认证后，会看到“身份已认证”的提示。此时我们的Trading 212 账户已经可以正常的进行交易了。

<div style="text-align: center;"><img src="/post_img/2024/04/trading212-08.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

<br>

--------------------------------------------

{% raw %} >>> 请点击此处继续浏览：{% endraw %}

## <a href="/posts/101a/" target="_blank"><b> 英国券商Trading 212注册和使用教程（下）</b></a>