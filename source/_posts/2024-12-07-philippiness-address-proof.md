---
title: 如何获取菲律宾地址证明
date: 2024-12-07 23:39:00
updated: 2024-12-07 23:39:00
categories: others
tags: [Philippines]
cover: /post_img/general/mail-box.png
permalink: posts/1027/
excerpt: 本文举例介绍一些获取菲律宾地址证明的方法
---

　　本文举例介绍一些获取菲律宾地址证明的方法。<!-- more -->

<img src="/post_img/2024/12/backyard.jpg" style="width: 100%; height: 250px; object-fit: cover;">

## 一、Own Bank银行账单

　　Own Bank可在线开户，申请简便。（参考：<a href="/posts/1025/" target="_blank">菲律宾Own Bank注册和使用教程</a>）

　　注册Own Bank并完成认证后，打开Own Bank APP，点击首页右上角的紫色圆圈。

<div style="text-align: center;"><img src="/post_img/2024/12/statement-01.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来，点击页面上方自己的名字。

<div style="text-align: center;"><img src="/post_img/2024/12/statement-02.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来，点击“Statement of Account”。

<div style="text-align: center;"><img src="/post_img/2024/12/statement-03.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　如果开户时间在上个月或者更早，则可以直接看到过往月份的银行账单。如果是本月开户的，则系统尚未生成账单，需要我们手动创建。

　　接下来，点击“Customize your statement”。

<div style="text-align: center;"><img src="/post_img/2024/12/statement-04.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来，选择一个日期范围，然后点击“Confirm”按钮。

<div style="text-align: center;"><img src="/post_img/2024/12/statement-05.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　当账单生成后，系统会向我们发送一条App内消息通知。回到App首页，点击页面右上角的铃铛图标，可以看到App内消息。

　　在消息列表中，点击最上方的“Your statement is successfully generated”这条消息，即可看到刚才创建的银行账单。

　　点击右上角的箭头图标，可以将账单下载到手机中。

<div style="text-align: center; display: flex; flex-wrap: wrap; justify-content: center;">
<img src="/post_img/2024/12/statement-06.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC; margin-left:5px; margin-right:5px;">
<img src="/post_img/2024/12/statement-07.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC; margin-left:5px; margin-right:5px;">
</div>

## 二、其他

（先开溜睡觉了(～﹃～)~zZ，其他的以后再慢慢补充）