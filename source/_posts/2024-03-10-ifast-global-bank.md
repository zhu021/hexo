---
title: 英国iFAST Global Bank数字银行注册和使用教程
date: 2024-03-10 23:00:00
updated: 2024-03-30 00:00:00
categories: bank
tags: [United Kingdom]
cover: /post_img/2024/03/ifast-01.jpg
permalink: posts/1010/
excerpt: 全套中国资料即可申请，有存款保险，有中文客服
---

　　本文介绍英国iFAST Global Bank数字银行的注册和使用，全套中国资料即可申请，有存款保险，有个人IBAN，支持6种货币，有中文客服。

　　（本文截图较多，如加载缓慢请耐心等待或尝试刷新）

<img src="/post_img/2024/03/ifast-02.jpg" style="width: 100%; height: 100%; max-height: 250px; object-fit: cover;">

## 一、功能简介

　　iFAST Group（奕丰集团）是一家从事财富管理和数字银行的新加坡上市公司（股票代码：AIY），在英国有数字银行业务，在新加坡、香港、马来西亚有券商业务，在中国有基金销售业务。本文介绍的iFAST Global Bank既是iFAST Group的数字银行业务。

　　iFAST Global Bank是英国FSCS(Financial Services Compensation Scheme)的成员，每个用户可享受8.5万英镑的存款保险。iFAST Global Bank的多币种账户支持英镑、欧元、美元、人民币、港币、新加坡元6个币种，提供个人IBAN，支持英国国内FPS转账和SWIFT国际转账。官方称计划支持欧洲的SEPA转账，有消息称2024年底会出借记卡。

## 二、注册和认证

### 1、注册账号

　　点击这里进入<a href="https://www.ifastgb.com/en/account-opening-home" rel="nofollow" target="_blank">iFAST Global Bank的注册页面</a>。

　　首先是“We need your consent before proceeding”声明页面，点击“Agree & Continue”。

　　接下来会询问我们需要注册账户的类型，我们选择“An Individual Applicant”，然后点击“Get Started”按钮。

　　接下来会询问我们是否是iFAST Group的现有客户，这里我们选择的是“No, I am new to iFAST”。

　　接下来会询问我们是否是iFAST Group或者其合作伙伴的雇员，这里我们选择“No, I am not”。

　　接下来是创建登录账号。

　　输入自己的EMail地址，点击旁边的“Verify”，然后点击弹窗中的“Send OTP”按钮。随后输入EMail中收到的6位数字验证码，点击“Submit”按钮。接着输入登录用户名、密码、重复密码，然后点击“Next”按钮。

### 2、身份认证

　　接下来“Important information about this account”的页面，点击“Next”按钮。

　　接下来是“Check your eligibility”页面。在“You live in（居住国）”和“You are currently located in（当前位置）”中根据自己的情况如实选择，然后点击“Next”。

>iFAST Global Bank不强制要求申请者居住在英国。如果申请者不居住在英国，建议“居住国”和“当前位置”都选择自己的国籍，便于接下来的KYC认证。

　　接下来是“FATCA Status”(美国的外国帐户税收遵从法案)页面，填写自己的税收居民信息。

　　与US有关的都选择“No”即可。“Tax Residency”选择“Yes”，然后在“Jurisdiction of Residence”中选择自己的税收居民国家，并在“TIN Number”中填写对应的税务识别号码。我们这里选择中国，并填写自己的身份证号（中国的税务系统使用居民的身份证号作为税务识别号码）。

　　如果你有一个以上的税收居民身份，可以点击“Add Additional Tax Residency”并继续填写。填写完成后，点击“Save & Continue”按钮。

　　接下来是“Personal Details”页面。如实填写自己的Contact Information、Personal Information、Residential Information。填写完成后，点击“Save & Continue”按钮。

>1、iFAST Global Bank支持外国人使用全套非英国的资料进行申请，所以我们如实填写自己的非英国手机号、居住地址、证件等信息即可。
2、“When did you move to this address?”的日期需要填写一个3年前的日期。如果当前的居住地址不满3年，需要补充填写之前的旧住址。
3、“Correspondence address same with residential address”和“Where would you like us to send your correspondence to”都保留默认选项即可。

　　接下来是“Financial Details”页面，如实填写自己的财务信息即可。填写完成后，点击“Save & Continue”按钮。

>申请银行账户时，雇佣状态选择“Employed”、收入来源选择“Salary”、主要财富选择“Savings”，一般更容易通过申请。

　　接下来是“Reason to open account”页面，开户理由一般选择“Savings/Investment”，其他的按自己的情况如实填写即可。填写完成后，点击“Save & Continue”按钮。

　　接下来是“Information about our products & services”页面，直接点击“Save & Continue”按钮即可。

　　接下来是“Review Application”页面，预览之前填写的申请信息。如果信息无误的话，点击“Agree & Continue”按钮。

　　接下来是“Important Information”页面，罗列了与开户相关的协议文档。依次点击每个文档名称旁边的箭头，确保文档的阅读状态由“Pending Review”变为“Opened”。全部点击完成后，点击两次“Agree & Continue”按钮。

　　接下来是“Upload Your Documents”页面。

　　“I'm a resident or live in”选择“Other”，然后点击“Continue”按钮。接下来依次拍照或上传我们的身份证件、地址证明、活体验证自拍、视频验证。

>1、上传的证件需要与先前填写的证件类型保持一致。
2、默认是通过自拍视频进行认证。但是根据网友们的反馈，有一定的概率会触发人工视频，需要预约时间后通过视频会议的方式进行验证。

　　接下来是“Application Submitted”页面，提示我们开户申请已正式提交，请耐心等待审核结果。

## 三、使用

iFAST Global Bank有APP版和网页版，本文以网页版为例。APP的操作步骤与网页版基本相同。

### 1、登录

　　审核通过后，就可以正常使用iFAST Global Bank了。

　　点击这里打开<a href="https://www.ifastgb.com/en/login" rel="nofollow" target="_blank">登录页面</a>，在登录框中输入EMail地址/登录名和密码，点击“Login”按钮。

　　接下来会要求我们选择一种登录认证方式。我们这里使用“Digital Token”，然后在列表中选择APP绑定的设备，点击“Authenticate Now”。

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-03.jpg" alt="" style="width: auto; height: auto; max-width: 600px;border:1px solid #CCCCCC;"></div>

　　此时页面中会显示一个“Digital Token Code”。

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-04.jpg" alt="" style="width: auto; height: auto; max-width: 600px;border:1px solid #CCCCCC;"></div>

　　接下来在手机上登录iFAST Global Bank的APP，依次点击左上角的“菜单” - “Digital Token”，确认APP上显示的“Digital Token”与网页中的一致后，点击APP底部的“Authorise”按钮。此时网页端就会成功登录。

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-05.jpg" alt="" style="width: auto; height: auto; max-width: 400px;border:1px solid #CCCCCC;"></div>

### 2、我的账户

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-06.jpg" alt="" style="width: auto; height: auto;border:1px solid #CCCCCC;"></div>

　　点击页面顶部的“My Account”，可以查看活期存款、定期存款、通知存款的账户信息和账单信息。

　　活期存款账户是一个支持6个币种的多币种账户，当前英镑、欧元、美元、港币、人民币、新币的年化利率分别是4.25%、2.50%、3.60%、3.25%、1.00%、2.20%。

　　点击页面中间的“Account Details”按钮，可以看到该账户的账号信息。该账户支持通过SWIFT和英国本土的FPS接收汇款。

　　在“My Consolidated Statement”中可以查看自己账户的月度账单、定期存款回单、汇款回单、利息回单。


### 3、转换货币

　　依次点击页面顶部的“Currency Converter” - “Currency Conversion”，可以在6种货币之间进行转换。

　　选择原货币，输入转换金额，选择目标币种，然后依次点击“Preview” - “Authorise”，即可完成货币的转换。在转换之前，可以在预览中看到汇率及转换后的金额。

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-07.jpg" alt="" style="width: auto; height: auto;border:1px solid #CCCCCC;"></div>

### 4、英国国内汇款

　　依次点击页面顶部的“Payment & Transfer” - “Payment within UK”，可以向英国国内的银行进行转账。向英国国内的银行汇英镑免费，汇欧元、美元、新元、港币、人民币的价格依次为12EUR、18USD、25SGD、145HKD、126CNY。

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-08.jpg" alt="" style="width: auto; height: auto;border:1px solid #CCCCCC;"></div>

　　在“Payment Detail”中选择币种、填写金额，选择转账目的、参考信息。在“Beneficiary Details”中填写收款人姓名、Sort Code、账号。

>1、如果汇款货币的账户余额不足，可以先转换货币，然后再进行同币种（Same Currency）汇款；也可以直接进行跨币种(Cross Currency)汇款。
2、向证券账户、加密货币交易所、第三方汇款公司等对公账户入金时，一般需要填写对方提供的Reference Number。将Reference Number填写到Payment Reference即可。

　　填写完成后，依次点击“Preview” - “Authorise”即可完成汇款。

### 5、国际汇款

　　依次点击页面顶部的“Payment & Transfer” - “International Payment”，可以进行SWIFT国际汇款。以同币种进行国际汇款，汇英镑、欧元、美元、新元、港币、人民币的价格依次为15GBP、12EUR、18USD、25SGD、145HKD、126CNY。

>使用跨币种汇款免收汇款手续费，只需承担货币转换的点差。

　　国际汇款的流程与国内汇款的流程基本相同，主要区别是“Beneficiary Details”中的Sort Code换成了SWIFT Code。另外部分国家要求汇款时填写收款人的IBAN号码。

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-09.jpg" alt="" style="width: auto; height: auto;border:1px solid #CCCCCC;"></div>

### 6、iFAST集团内部平台互转

　　iFAST支持在iFAST Global Bank、FSMOne SG、FSMOne HK、FSMOne MY之间免费转账。

　　在首次转账之前，需要先通过EMail联系iFAST Global Bank的客服绑定账号。参考：

>Email地址: clienthelp#ifastgb.com (将#替换@)
邮件标题: Apply for account linkage
邮件内容: 
Apply for account linkage with FSMOne SG
Name: xxxxxx
iFAST Global Bank Profile ID: xxxxxx
FSMOne SG Profile ID: xxxxxx

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-10.jpg" alt="" style="width: auto; height: auto;border:1px solid #CCCCCC;"></div>

### 7、定期存款和通知存款

　　点击页面顶部的“Apply Deposit”，可以查看不同币种、不同期限的固定存款、通知存款的利率。点击对应的“Apply Now”即可办理存款。

<div style="text-align: center;"><img src="/post_img/2024/03/ifast-11.jpg" alt="" style="width: auto; height: auto;border:1px solid #CCCCCC;"></div>