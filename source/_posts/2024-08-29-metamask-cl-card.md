---
title: MetaMask虚拟Master卡注册和使用教程
date: 2024-08-31 19:35:00
updated: 2024-08-31 19:35:00
categories: Cryptocurrency
tags: [United Kingdom, Europe, card, Master]
cover: /post_img/2024/09/metamask-01.jpg
permalink: posts/1024/
excerpt: 虚拟Master卡，加密货币入金
---

　　本文介绍MetaMask的Master卡的注册和使用方法。（本文截图较多，如加载缓慢请耐心等待或尝试刷新）

<img src="/post_img/2024/09/metamask-02.jpg" style="width: 100%; height: 250px; object-fit: cover;">

## 一、功能简介

　　MetaMask是全球用户量最大的非托管型加密货币钱包客户端之一。MetaMask目前与Crypto Life Card合作推出了Master卡（下称“狐狸卡”），目前面向英国和部分欧盟国家的用户开放注册（当地手机号+当地地址+中国护照可注册）。

　　狐狸卡目前仅推出了虚拟卡，该卡支持加密货币入金（目前支持Linea链上的USDC、USDT、WETH），卡类型为预付费卡。英国用户的卡BIN归属地为英国，暂不知欧盟用户的卡BIN。

　　狐狸卡的独特之处在于它无需提前划转资金进行充值，只需先进行授权，随后每次消费时才会从绑定的钱包中扣款。

## 二、申请狐狸卡

### 1、创建账号

　　打开<a href="https://portfolio.metamask.io/card" rel="nofollow" target="_blank">MetaMask的狐狸卡介绍页面</a>，点击蓝色的“Get Started”按钮。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-03.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>1、如果访问者的IP归属地不属于狐狸卡的开放注册国家，则页面会显示“Join the Waitlist”按钮，而不是“Get Started”按钮。
2、如果页面显示“Page Not Found”，请尝试（1）关闭设备的定位开关；(2)更换IP；（3）更换浏览器；（4）更换设备。

　　接下来会跳转到Crypto Life官网的用户注册页面（该注册页面仅针对狐狸卡）。

　　输入自己的EMail地址，点击“Confirm your email address”按钮。接着输入EMail中收到的验证码，点击“Confirm”按钮。

<div style="text-align: center; display: flex; flex-wrap: wrap; justify-content: center;">
<img src="/post_img/2024/09/metamask-04.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC; margin-left:5px; margin-right:5px;">
<img src="/post_img/2024/09/metamask-05.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC; margin-left:5px; margin-right:5px;">
</div>

　　接下来输入自己的手机号，点击“Confirm your phone number”按钮。接着输入收到的短信验证码，点击“Confirm”按钮。

<div style="text-align: center; display: flex; flex-wrap: wrap; justify-content: center;">
<img src="/post_img/2024/09/metamask-06.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC; margin-left:5px; margin-right:5px;">
<img src="/post_img/2024/09/metamask-07.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC; margin-left:5px; margin-right:5px;">
</div>

　　接下来输入为账号设置的密码，点击“Create password & continue”按钮。

<div style="text-align: center; display: flex; flex-wrap: wrap; justify-content: center;">
<img src="/post_img/2024/09/metamask-08.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;">
</div>

### 2、身份认证

　　点击“Verify now with Veriff”按钮，进入身份认证流程。身份认证服务由第三方供应商Veriff提供。

　　按照页面的提示上传自己的证件照和自拍照。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-09.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

### 3、完善个人信息

　　按照页面的提示完善居住地址等个人信息，然后点击“Confirm your details”按钮。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-10.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>无需提供地址证明，地址无需信用记录。

### 4、开启2FA

　　2FA（two factor authentication，双因素认证）是一种密码之外的额外账户认证方式，使用任意一个2FA APP（例如Microsoft Authenticator 、google authenticator）与自己的账户进行绑定，之后2FA APP每分钟会生成一个新的动态验证码。为账户开启2FA后，当我们进行登录或者其他敏感操作时，可能会被要求输入2FA APP中的动态验证码。

　　这里Crypto Life也要求我们为新创建的账号开启2FA。页面会显示一个二维码，打开2FA APP扫描这个二维码，即可完成账号绑定。接着点击二维码下方的“Continue”按钮。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-11.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来在下图所示的文本框中输入2FA APP中显示的动态验证码，然后点击“Verify”按钮。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-12.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　至此，我们已经完成了申请流程。

### 三、提前准备资金

　　在授权扣款时，需要确保我们的钱包内有相应的加密货币，所以需要进行提前准备。

　　狐狸卡目前仅支持使用Linea链上的USDC、USDT、WETH进行扣款。而Linea链是MetaMask推出的一个网络，相对小众，所以一般还需要我们将自己钱包内其他链上的加密货币桥接转换到Linea链上。

　　通过MetaMask的APP或者浏览器插件打开自己的钱包，点击“Bridge”（跨链桥、桥接）按钮，进入MetaMask官网的桥接页面（如下图所示）。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-13.jpg" alt="" style="width:100%; max-width: 600px; height: auto; border:1px solid #CCCCCC;"></div>

　　在“From this network”中选择现有资产所在的网络，在“You send”中选择需要转换的币种并填入需要转换的数量，在“To this network”中选择`Linea`。此时系统会向供应商询价，并返回一个报价最优的供应商。也可以手动点击报价右下角的“Choose different quote”，查看其他供应商的报价。

　　接着点击“Connect Wallet”进行转换。如果一切顺利的话，我们钱包里的Linea网络中现在就会有相应的加密货币了。

>1、转换前和转换后应当保持相同的币种。
2、首次转换时，建议同时勾选“Add gas on Linea”（非必须），顺便买一些Linea链上的ETH币，预备以后在Linea链上转账时支付gas费。



## 三、设置扣款授权金额

　　要想启用我们的狐狸卡，需要绑定一个钱包地址，然后授权狐狸卡从这个钱包中扣费。

　　进入<a href="https://metamask.withcl.com/card/transaction" rel="nofollow" target="_blank">狐狸卡的首页</a>，点击下方的“Manage Limit”按钮。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-14.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　首次设置的时候，MetaMask会赠送我们少量ETH用于支付扣款授权操作时所需的gas费。点击“Claim your ETH”继续。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-15.jpg" alt="" style="width:100%; max-width: 500px; height: auto; border:1px solid #CCCCCC;"></div>

　　接着点击“Manage spending limit”。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-16.jpg" alt="" style="width:100%; max-width: 500px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来，根据自己的喜好选择一种授权方式，然后点击“Approve now”。本文中以Approve a special limit为例。

　　Automatically approve spending表示自动授权，以后无需再次授权；Approve a special limit表示仅授权特定金额，累计消费完这个金额后还需再次授权。

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-17.jpg" alt="" style="width:100%; max-width: 350px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来，会跳转到MetaMask钱包。在其中输入本次授权的金额上限，然后点击“下一步”按钮。接着继续点击“批准”按钮。

<div style="text-align: center; display: flex; flex-wrap: wrap; justify-content: center;">
<img src="/post_img/2024/09/metamask-18.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC; margin-left:5px; margin-right:5px;">
<img src="/post_img/2024/09/metamask-19.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC; margin-left:5px; margin-right:5px;">
</div>

<div style="text-align: center;"><img src="/post_img/2024/09/metamask-20.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>如果手机端无法成功联接MetaMask钱包，请尝试在电脑端操作。电脑端需安装<a href="https://metamask.io/download/" rel="nofollow" target="_blank">MetaMask浏览器插件</a>。

　　至此，就完成了扣款的授权，接下来就可以用卡消费了。

## 四、用卡

　　此时可以看到卡的可用余额是9.89USDC，正好等于先前桥接兑换的USDC的金额。

<div style="text-align: center;">
<img src="/post_img/2024/09/metamask-14.jpg" alt="" style="width:100%; max-width: 300px; height: auto; border:1px solid #CCCCCC;">
</div>


　　点击“View card details”按钮，可以看到卡号、有效期、CVV。

　　在支持Master的商户网购时，输入自己的卡信息即可完成支付。也可绑定微信、支付宝、Google Pay、Apply Pay等第三方支付工具使用（未实测）。