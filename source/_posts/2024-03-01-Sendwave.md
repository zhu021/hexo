---
title: Sendwave - 小众货币汇款神器
date: 2024-03-01 22:00:00
updated: 2024-03-01 22:00:00
categories: remittance
#tags: [money transfer]
cover: /post_img/2024/03/sendwave-01.png
permalink: posts/1008/
excerpt: 汇款可低至0费率，现在注册送10英镑/欧元/美元
---

　　小众货币汇款神器Sendwave，汇款可低至0费率，现在注册送10英镑/欧元/美元。本文介绍注册流程和10英镑代金券的使用方法。

　　（本文截图较多，如加载缓慢请耐心等待或尝试刷新）

<img src="/post_img/2024/03/sendwave-02.png" style="width: 100%; height: 250px; object-fit: cover;">

## 一、APP简介

　　Sendwave是WordRemit旗下的一个汇款工具。根据官网信息，WordRemit在美国、加拿大、英国、比利时拥有公司实体和汇款相关的金融资质。

　　Sendwave支持从英国、欧洲、美国、加拿大向泰国、尼日利亚、墨西哥等约29个国家进行汇款，支持电子钱包、银行账户、现金等收款方式，一些收款国家和收款方式低至0费率。（参考信息：<a href="https://www.sendwave.com/en/countries" rel="nofollow" target="_blank">Sendwave官网 - 支持的国家和货币</a>）
<div style="text-align: center;"><img src="/post_img/2024/03/sendwave-03.png" alt="" style="width: auto; height: auto; border:1px solid #CCCCCC;"></div>

## 二、注册流程

### 1、安装APP

　　在苹果App Store、谷歌Play Store或者其他应用商店搜索`Sendwave`，然后下载并安装。 

### 2、注册

　　打开App，点击“Sign up”按钮。

<div style="text-align: center;"><img src="/post_img/2024/03/sendwave-04.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接着依次输入自己的姓名、EMail、手机号、密码，并在“Where are you sending to first”中选择“Philippines”，然后点击“Sign up”按钮。

>1、填写不同国家的手机号，会对应不同的WordRemit法律实体，从而适用不同的付款货币、付款方式、服务条款。本文以填写美国手机号为例。（参考文章：<a href="/categories/sim-card/" rel="nofollow" target="_blank">各国手机卡</a>）
2、本文以向菲律宾汇款为例，所以首次收款国家选择“菲律宾”。实际可以随意选择，对后续的使用没有任何影响。

　　接下来Sendwave会向我们填写的手机号发送一个短信验证码。输入这个短信验证码，账号就会完成注册。此时APP会询问“是否保存密码，开启生物识别登录方式”，我们可以根据自己的喜好作选择。

## 三、使用代金券

### 1、领取代金券

　　注册完成账号后，APP会自动跳转到汇款界面。我们先退出汇款界面，返回APP的首页。

　　接下来依次点击APP底部的“Me”(小人图标) - “Enter Promo Code”，输入`Q7YFK`，然后点击“Apply Code”按钮。

<div style="text-align: center;"><img src="/post_img/2024/03/sendwave-05.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来点击APP底部的“Send Money”(纸飞机图标)，可以看到一行绿色的文字“$10 credit applied”。

<div style="text-align: center;"><img src="/post_img/2024/03/sendwave-06.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>因为本文中是使用美国手机号注册的，所以得到的是美元代金券。如果使用其他国家的手机号注册，会相应得到英镑、欧元代金券。

### 2、填写汇款信息

　　接下来点击“RECIPIENT” - “Add a new recipient”，参照下边的样子填写收款人的信息，然后点击“Save”按钮。如果遇到APP询问读取联系人权限，直接点击“拒绝”即可。

<div style="text-align: center;"><img src="/post_img/2024/03/sendwave-07.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>这里我们以向菲律宾的GCash电子钱包汇款为例。如果你有Sendwave支持的任何国家的收款方式，均可自行填写。

<div style="text-align: center;"><img src="/post_img/2024/03/sendwave-08.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

　　接下来APP会返回“Send Money”界面，我们在“You send”中填写汇款金额。此时可以看到“They receive”中的金额会远高于我们填写的汇款金额，这是因为收款金额中包含了10美元的代金券金额。也就是说代金券中的金额会以汇款的形式随同我们自己的汇款一同汇给收款人。

>1、最低的汇款金额是1英镑/美元/欧元。
2、部分收款国家不支持使用代金券，请注意识别，此时不会显示绿色的文字“$10 credit applied”。

### 3、填写付款信息

　　接下来点击“Continue to send”按钮，然后在“Add Payment Method”界面中填写付款银行卡的信息以及资金来源，然后点击“Continue”按钮。

<div style="text-align: center;"><img src="/post_img/2024/03/sendwave-09.jpg" alt="" style="width: 300px; height: auto; border:1px solid #CCCCCC;"></div>

>根据APP的提示，付款支持使用美国、英国、加拿大、意大利、西班牙、法国、比利时、爱尔兰发行的借记卡。建议使用与自己注册手机号的国家相同的借记卡。

### 4、身份认证

　　如果Card Check顺利的话，接下来APP会提示我们进行身份认证，按照APP的提示进行完成认证即可。

　　如果要求提供居住国的信息，与注册手机号的国家保持一致。如果要求提供证件，使用自己的护照，或者居住国的身份证、驾照、居留许可等支持的证件。

### 5、汇出款项

　　完成身份认证后，Sendwave会正式从之前的银行卡中扣款并汇出款项，汇款的金额包含银行卡中扣除的金额以及代金券中的金额。

　　不同的收款国家、收款方式的到账时间可能会不尽相同。一般电子钱包会在几分钟内到账，而银行汇款会相对较慢。

>如果你没有Sendwave支持的收款方式，可以在闲鱼等平台挂一个代充值的链接，通过帮人充值的方式变现。如果你使用本文提供的Promo Code，也可以联系阿猪提供帮助。