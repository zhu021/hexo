---
title: 老挝对中国团体游免签
date: 2024-06-28 23:00:00
updated: 2024-06-28 23:00:00
categories: visa policy
tags: [Laos]
cover: /post_img/2024/06/laos.jpg
permalink: posts/1018/
excerpt: 持普通护照通过老挝的旅游公司可15天免签
---

<img src="/post_img/2024/06/laos.jpg" style="width: 100%; height: 250px; object-fit: cover;">

　　消息来源：<a href="https://news.cctv.com/2024/06/27/ARTIMvrm7hEMeniJ7QUDMSiE240627.shtml" rel="nofollow" target="_blank">央视新闻</a>

　　当地时间6月26日，老挝新闻文化旅游部部长签发了《2024老挝旅游年关于对特定游客签证政策实施细则》29号文件。

　　根据文件内容，中国内地（大陆）及港澳台游客可以通过旅游公司组织，获取普通护照15天免签的政策。行程需要由老挝旅游公司组织并获得新闻文化旅游部的许可。政策执行时间从2024年7月1日起到2024年12月31日。

>根据公开信息，个人游仍需办理签证（可电子签证）或者落地签。（参考链接：<a href="http://cs.mfa.gov.cn/zggmcg/ljmdd/yz_645708/lw_646826/" rel="nofollow" target="_blank">中国领事服务网</a>）