---
#title: About
date: 2023-07-02 16:02:51
order: 3
---

## 关于本博客：
记录赛博居民的日常折腾

## 关于博主：
阿猪

## 联系方式：

<div style="display: inline-block;">
<i class="fas fa-envelope"></i>&nbsp;&nbsp;<div id="ct1" style="display: inline-block;"></div>
<div id="ct2" style="display: inline-block;"></div><div id="ct3" style="display: inline-block;"></div>
</div>
<script type="text/javascript">
var array = ["\u0061", "\u0062", "\u0065", "\u0068", "\u0069", "\u0070", "\u0073", "\u0074", "\u0075", "\u007a", "\u002e", "\u0040"];
var order = [6,9,2];
var output = document.getElementById("ct1");
for (var i = 0; i < order.length; i++) {
  output.innerHTML += array[order[i]-1];
}
var order2 = [12];
var output2 = document.getElementById("ct2");
for (var i = 0; i < order2.length; i++) {
  output2.innerHTML += array[order2[i]-1];
}
var order3 = ["1", "10", "4", "9", "11", "7", "5", "8", "3"];
var output3 = document.getElementById("ct3");
for (var i = 0; i < order3.length; i++) {
  output3.innerHTML += array[order3[i]-1];
}
</script>

<div><i class="fab fa-telegram"></i>&nbsp;&nbsp;zhu021</div>

## 交流群：

<div><i class="fab fa-telegram"></i>&nbsp;&nbsp;<a href="https://t.me/+jKHgRYRqg7NhNTBl" rel="nofollow" target="_blank">TG群</a></div>

<div><i class="fab fa-qq"></i>&nbsp;&nbsp;<a href="http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=UEjmmFBanCvaFltucE7a2NS3w0ir7ku8&authKey=Qk%2B%2B4NlL9CGWJZsPAwOywhrOaz5uInGaLLPqeCs9Yn4qgqFyrWe%2Bfd6gYhV9CXve&noverify=0&group_code=808322094" rel="nofollow" target="_blank">QQ群：808322094</a></div>

<div><i class="fab fa-weixin"></i>&nbsp;&nbsp;微信群（加微信azhu6713，拉你入群）</div>